import React, { Component } from 'react';
import Player from '../components/Player';
import PreloaderLayer from '../components/PreloaderLayer';
import { onFullscreenChange } from '../helpers/events';

class OnlyAds extends Component {
  state = {
    isPageReady: false,
  };

  constructor(props) {
    super(props);
    this.onReady = this.onReady.bind(this);
  }

  onReady() {
    this.setState({ isPageReady: true });
  }

  render() {
    const { isPageReady } = this.state;
    const { ads } = this.props;
    const adList = ads.map((ad) => ({
      sources: [{
        src: ad.src,
        type: 'video/mp4',
      }]
    }));
    return (
      <div>
        (!isPageReady && <PreloaderLayer />)
        <Player
          controls
          playlist={adList}
          onReady={this.onReady}
          onPlayerEnded={() => window.location.href = '/'}
          onError={() => window.location.href = '/'}
          onFullscreenExit={() => onFullscreenChange(() => window.location.href = '/')}
        />
      </div>
    )
  }
}
export default OnlyAds;