import React, { Component } from 'react';
import PreloaderLayer from '../components/PreloaderLayer';
import { onFullscreenChange } from '../helpers/events';
import SettingsDialog from '../components/SettingsDialog';
import Player from '../components/Player';

class Films extends Component {
  state = {
    isPageReady: false,
  };

  constructor(props) {
    super(props);
    this.onReady = this.onReady.bind(this);
  }

  onReady() {
    this.setState({ isPageReady: true });
  }

  render() {
    const { isPageReady } = this.state;
    const {
      ads,
      films,
      selectedGenre
    } = this.props;
    const adOptions = {
      adTimeout: Number.parseFloat(`${SettingsDialog.getAdPeriod()}.0`) * 60000,
      adList: ads.map((ad) => ({
        src: ad.src,
        type: 'video/mp4',
    }))};
    const selectedFilmIndex = this.props.location.state.index;
    const selectedFilmGenreIndex = films.findIndex(elem => elem.genre === selectedGenre)
    var currentFilmList = films[selectedFilmGenreIndex].filmList;
    const filmListInOrder = currentFilmList.splice(selectedFilmIndex).concat(currentFilmList);
    const playlist = filmListInOrder.map((film) => ({
      sources: [{
        src: film.src,
        type: 'video/mp4',
      }]
    }));
    return (
      <div>
        (!isPageReady && <PreloaderLayer />)
        <Player
          controls
          playlist={playlist}
          adOptions={adOptions}
          onReady={this.onReady}
          onPlayerEnded={() => window.location.href = '/'}
          onError={() => window.location.href = '/'}
          onFullscreenExit={() => onFullscreenChange(() => window.location.href = '/')}
        />
      </div>
    )
  }
}
export default Films;