import React, { Component } from 'react'
import { Link } from 'react-router-dom';
import { withStyles, MuiThemeProvider, createMuiTheme } from '@material-ui/core/styles';
import {
  Drawer,
  AppBar,
  CssBaseline,
  Toolbar,
  List,
  Typography,
  Divider,
  ListItem,
  ListItemIcon,
  ListItemText,
} from '@material-ui/core';
import FolderIcon from '@material-ui/icons/Folder';
import VideoLibraryIcon from '@material-ui/icons/VideoLibrary';
import SettingsIcon from '@material-ui/icons/Settings';
import PlayArrowIcon from '@material-ui/icons/PlayCircleFilled';
import SettingsDialog from '../containers/SettingsDialog';

class Genres extends Component {
  componentWillMount() {
    const { loadVideoLib } = this.props;
    loadVideoLib(); 
  }

  render() {
    const {
      classes,
      showSettingsDialog,
      setGenre,
      selectedGenre,
      films,
    } = this.props;
    const genresList = films.map(film => film.genre);
    let filmList = [];
    const filmListByGenre = films.find(film => film.genre === selectedGenre);
    if (selectedGenre && filmListByGenre && filmListByGenre.hasOwnProperty('filmList')) {
      filmList = filmListByGenre.filmList;
    }
    console.log(filmList);
    return (
      <MuiThemeProvider theme={theme}>
        <div className={classes.root}>
          <CssBaseline />
          <AppBar position="fixed" className={classes.appBar}>
            <Toolbar>
              <Typography variant="h1" color="inherit" noWrap>
                Эстетик / Фильмы
            </Typography>
            </Toolbar>
          </AppBar>
          <Drawer
            className={classes.drawer}
            variant="permanent"
            classes={{
              paper: classes.drawerPaper,
            }}
          >
            <div className={classes.toolbar} />
            <Link className={classes.listLink} to="/onlyads">
              <ListItem button>
                <ListItemIcon>
                  <PlayArrowIcon fontSize="large" />
                </ListItemIcon>
                <ListItemText primary={'Только реклама'} className={classes.listItemText} />
              </ListItem>
            </Link>
            {
              genresList.map((genre, index) => (
                <List onClick={() => { setGenre(genre) }} key={String(index)}>
                  <ListItem button key={String(index)} selected={selectedGenre === genre}>
                    <ListItemIcon>
                      <FolderIcon fontSize="large" />
                    </ListItemIcon>
                    <ListItemText primary={genre} className={classes.listItemText} />
                  </ListItem>
                </List>
              ))
            }
            <Divider />
            <List onClick={() => { showSettingsDialog(); }}>
              <ListItem button key={'Настройки'}>
                <ListItemIcon><SettingsIcon fontSize="large" /></ListItemIcon>
                <ListItemText primary={'Настройки'} className={classes.listItemText} />
              </ListItem>
            </List>
          </Drawer>
          <main className={classes.content}>
            <div className={classes.toolbar} />
            {
              (filmList.length > 0) ? (
                <List>
                  {
                    filmList.map((film, index) => {
                      return (
                        <Link className={classes.listLink} to={{ pathname: "/films", state: { url: film.src } }} key={String(index)}>
                          <Typography>
                            <ListItem button key={index} className={classes.listItemText}>
                              <ListItemIcon>
                                <VideoLibraryIcon fontSize="large" />
                              </ListItemIcon>
                              <ListItemText primary={film.name} className={classes.listItemText} />
                            </ListItem>
                          </Typography>
                        </Link>
                      );
                    })
                  }
                </List>
              ) : (
                <div className={classes.chooseGenreContainer}>
                  <Typography variant="h1" className={classes.chooseGenreText}>
                    {
                      !selectedGenre ? 'Для проигрывания фильмов выберите жанр'
                          : `В жанре "${selectedGenre}" на данный момент нет фильмов`
                    }
                  </Typography>
                </div>
              )
            }
          </main>
          <SettingsDialog />
        </div>
      </MuiThemeProvider>   
    )
  }
}

const theme = createMuiTheme({
  typography: {
    fontSize: '100%',
  },
});

const styles = theme => ({
  root: {
    display: 'flex',
  },
  appBar: {
    zIndex: theme.zIndex.drawer + 1,
  },
  drawer: {
    width: 'auto',
    minWidth: '400px',
    flexShrink: 0,
  },
  drawerPaper: {
    width: 'auto',
    minWidth: '400px',
  },
  content: {
    flexGrow: 1,
    padding: theme.spacing.unit * 3,
    paddingLeft: theme.spacing.unit * 10,
  },
  toolbar: theme.mixins.toolbar,
  listLink: {
    textDecoration: 'none',
  },
  listItemText: {
    fontSize: '1.5em',
  },
  chooseGenreContainer: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    textAlign: 'center',
    paddingTop: '25%',
  },
  chooseGenreText: {
    fontSize: '3.5em',
    color: '#c4c4c4',
    lineHeight: 1.2,
  },
});

export default withStyles(styles)(Genres);
