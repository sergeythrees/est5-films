const initialState = {
  selectedGenre: null,
};

export const genres = (state = initialState, action) => {
  switch (action.type) {
    case 'SET_GENRE':
      return Object.assign({}, state, {
        selectedGenre: action.payload,
      });
    default:
      return state;
  }
};
