const initialState = {
  films: [],
  ads: [],
  isLoading: false,
  isLoadingError: false,
};

export const videoLib = (state = initialState, action) => {
  switch (action.type) {
    case 'REQUEST_LOAD_VIDEOLIB':
      return Object.assign({}, state, {
        isLoading: true,
      });
    case 'SUCCESS_LOAD_VIDEOLIB':
      return Object.assign({}, state, {
        isLoading: false,
        isLoadingError: false,
        films: action.payload.films,
        ads: action.payload.ads,
      });
    case 'ERROR_LOAD_VIDEOLIB':
      return Object.assign({}, state, {
        isLoading: false,
        isLoadingError: true,
        videoLib: [],
      });
    default:
      return state;
  }
};
