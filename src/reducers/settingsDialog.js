const initialState = {
  isVisible: false,
};

export const settingsDialog = (state = initialState, action) => {
  switch (action.type) {
    case 'SHOW_SETTINGS_DIALOG':
      return Object.assign({}, state, {
        isVisible: true,
      });
    case 'HIDE_SETTINGS_DIALOG':
      return Object.assign({}, state, {
        isVisible: false,
      });
    default:
      return state;
  }
};
