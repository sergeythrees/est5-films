import { combineReducers } from 'redux';
import { settingsDialog } from './reducers/settingsDialog';
import { genres } from './reducers/genres';
import { videoLib } from './reducers/videoLib';

export default combineReducers({
  settingsDialog,
  genres,
  videoLib,
});