import React, { Component } from 'react';
import Genres from './containers/Genres';
import Films from './containers/Films';
import OnlyAds from './containers/OnlyAds';
import { BrowserRouter, Route } from 'react-router-dom';
import './App.css';
import store from './store';
import 'typeface-roboto';

class App extends Component {
  render() {
    return (
      <BrowserRouter history={store.history}>
        {/* <AnimatedSwitch
          atEnter={{ opacity: 0 }}
          atLeave={{ opacity: 0 }}
          atActive={{ opacity: 1 }}
          className="switch-wrapper"
        > */}
          <Route exact path="/" component={Genres} />
          <Route path="/films" component={Films} />
          <Route path="/onlyads" component={OnlyAds} />
        {/* </AnimatedSwitch> */}
      </BrowserRouter>
    );
  }
}

export default App;
