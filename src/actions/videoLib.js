const config = process.env.NODE_ENV === 'production'
  ? require('../production.config.json')
  : require('../dev.config.json');

export function requestLoadVideolib() {
  return {
    type: 'REQUEST_LOAD_VIDEOLIB',
  };
}

export function successLoadVideolib(videoLib) {
  return {
    type: 'SUCCESS_LOAD_VIDEOLIB',
    payload: videoLib,
  };
}

export function errorLoadVideolib() {
  return {
    type: 'ERROR_LOAD_VIDEOLIB',
  };
}

export const loadVideoLib = () => (dispatch) => {
  dispatch(requestLoadVideolib());
  fetch(`${config.api_host}/films`)
    .then((response) => response.json())
    .then((videoLib) => dispatch(successLoadVideolib(videoLib)))
    .catch(() => dispatch(errorLoadVideolib()));
}



