export function setGenre(genre) {
  return {
    type: 'SET_GENRE',
    payload: genre,
  };
}
