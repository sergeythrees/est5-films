export function showSettingsDialog() {
  return {
    type: 'SHOW_SETTINGS_DIALOG',
  };
}

export function hideSettingsDialog() {
  return {
    type: 'HIDE_SETTINGS_DIALOG',
  };
}