import React from 'react';
import videojs from 'video.js'
import 'videojs-playlist/dist/videojs-playlist.js';
import 'video.js/dist/video-js.css';
import 'videojs-contrib-ads/dist/videojs.ads.css';
import 'videojs-contrib-ads/dist/videojs.ads.js';
import './AdPlugin.js';

export default class Player extends React.Component {

  setFullScreen() {
    const {
      onReady,
      onFullscreenExit
    } = this.props;
    setTimeout(() => {
      document.getElementsByClassName('vjs-fullscreen-control')[0].click();
      setTimeout(() => {
        if (onFullscreenExit) onFullscreenExit();
      }, 1000);
      onReady();
    }, 500);
  }

  componentDidMount() {
    const {
      adOptions,
      playlist
    } = this.props;

    this.player = videojs(this.videoNode, this.props);

    if (adOptions) {
      this.player.adsPlugin(adOptions);
    }

    this.player.controls(true);

    this.player.playlist(playlist);
    this.player.playlist.repeat(true);
    this.player.playlist.autoadvance(0);

    var playPromise = this.player.play();
    playPromise.then(() => {
      })
      .catch(() => {
      });

    this.setFullScreen();
  }

  componentWillMount() {
    const { playlist, onError } = this.props;
    if (!playlist || !Array.isArray(playlist) || playlist.length === 0) {
      onError();
    }
  }

  componentWillUnmount() {
    if (this.player) {
      this.player.dispose()
    }
  }

  render() {
    return (
      <div>
        <div data-vjs-player>
          <video
            ref={node => this.videoNode = node}
            className="video-js"
          ></video>
        </div>
      </div>
    )
  }
}