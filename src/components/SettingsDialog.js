import React from 'react';
import {
  Button,
  TextField,
  Dialog,
  DialogActions,
  DialogContent,
  DialogTitle,
} from '@material-ui/core';


export default class SettingsDialog extends React.Component {
  static AD_PLAY_PERIOD_KEY = 'adPlayPeriod';

  static getAdPeriod() {
    return localStorage.getItem(SettingsDialog.AD_PLAY_PERIOD_KEY) || 20;
  };

  state = {
    adPlayPeriod: SettingsDialog.getAdPeriod(),
  };

  onChangeAdPlayPeriod = (e) => {
    this.setState({ adPlayPeriod: e.target.value });
  };

  onSave = () => {
    const { hideDialog } = this.props;
    const { adPlayPeriod } = this.state;
    localStorage.setItem(SettingsDialog.AD_PLAY_PERIOD_KEY, adPlayPeriod);
    hideDialog();
  };

  render() {
    const {
      isVisible,
      hideDialog,
    } = this.props;
    const { adPlayPeriod } = this.state;
    const currentAdPlayPeriodKey = adPlayPeriod || SettingsDialog.AD_PLAY_PERIOD_KEY;
    return (
      <div>
        <Dialog
          open={isVisible}
          onClose={hideDialog}
          aria-labelledby="form-dialog-title"
          fullWidth="500"
        >
          <DialogTitle id="form-dialog-title">Настройки</DialogTitle>
          <DialogContent>
            <TextField
              margin="dense"
              id="adPlayPeriod"
              label="Период запуска рекламы (мин.)"
              type="number"
              defaultValue={currentAdPlayPeriodKey}
              min
              fullWidth
              onChange={this.onChangeAdPlayPeriod}
            />
          </DialogContent>
          <DialogActions>
            <Button onClick={hideDialog} color="primary">
              Закрыть
            </Button>
            <Button onClick={this.onSave} color="primary">
              Сохранить
            </Button>
          </DialogActions>
        </Dialog>
      </div>
    );
  }
}
