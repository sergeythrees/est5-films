import React from 'react';

const PreloaderLayer = () => (
  <div style={{
    width: '100%',
    backgroundColor: '#000',
    zIndex: 999,
    position: 'absolute',
    top: 0,
    bottom: 0,
  }}></div>
);

export default PreloaderLayer;