import videojs from 'video.js';
(function (window, document, vjs, undefined) {

  var registerPlugin = vjs.registerPlugin || vjs.plugin;

  registerPlugin('adsPlugin', function (adOptions) {

    var
      player = this,

      currentAdIndex = -1,
      playPreroll = false,
      playPostroll = false,
      isAdWaiting = false,
      adTimeout = adOptions.adTimeout,
      timeoutId,

      getNextAdMedia = function () {
        if (adOptions.adList.length === 1) {
          currentAdIndex = 0;
        }
        else if (currentAdIndex === (adOptions.adList.length - 1)) {
          currentAdIndex = 0;
        }
        else {
          currentAdIndex++;
        }
        return adOptions.adList[currentAdIndex];
      },

      playAd = function () {

        player.ads.startLinearAdMode();

        var media = getNextAdMedia();

        player.src(media);
        player.trigger('ads-ad-started');

        player.one('adended', function () {
          player.trigger('ads-ad-ended');
          player.ads.endLinearAdMode();
          isAdWaiting = false
        });
      };

    player.ads();

    player.on('adsready', function () {
      if (!playPreroll) {
        player.trigger('nopreroll');
      }
    });

    player.on('contentchanged', function () {
      clearTimeout(timeoutId);
      isAdWaiting = false;
    });

    player.on('readyforpostroll', function () {
      if (playPostroll) {
        playAd();
      } else {
        player.trigger('nopostroll');
      }
    });

    player.on('readyforpreroll', function () {
      if (playPreroll) {
        playAd();
      }
    });

    player.on('timeupdate', function (event) {
      if (!isAdWaiting) {
        isAdWaiting = true;
        timeoutId = setTimeout(() => {
          playAd();
        }, (adTimeout));
      }
    });
  });

})(window, document, videojs);