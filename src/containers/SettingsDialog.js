import { connect } from 'react-redux';
import {
  showSettingsDialog,
  hideSettingsDialog,
} from '../actions/settingsDialog';
import SettingsDialog from '../components/SettingsDialog';

const mapState = state => ({
  isVisible: state.settingsDialog.isVisible,
});

const mapDispatch = dispatch => ({
  showDialog: () => dispatch(showSettingsDialog()),
  hideDialog: () => dispatch(hideSettingsDialog()),
});

export default connect(mapState, mapDispatch)(SettingsDialog);
