import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import { showSettingsDialog } from '../actions/settingsDialog';
import { setGenre } from '../actions/genres';
import { loadVideoLib } from '../actions/videoLib';
import Genres from '../pages/Genres';

const mapStateToProps = state => ({
  selectedGenre: state.genres.selectedGenre,
  films: state.videoLib.films,
});


const mapDispatchToProps = dispatch => ({
  showSettingsDialog: () => dispatch(showSettingsDialog()),
  setGenre: (genre) => dispatch(setGenre(genre)),
  loadVideoLib: bindActionCreators(loadVideoLib, dispatch),
});

export default connect(mapStateToProps, mapDispatchToProps)(Genres);
