import { connect } from 'react-redux';
import OnlyAds from '../pages/OnlyAds';

const mapStateToProps = state => ({
  ads: state.videoLib.ads,
});


const mapDispatchToProps = dispatch => ({
});

export default connect(mapStateToProps, mapDispatchToProps)(OnlyAds);
