import { connect } from 'react-redux';
import Films from '../pages/Films';

const mapStateToProps = state => ({
  ads: state.videoLib.ads,
  films: state.videoLib.films,
  selectedGenre: state.genres.selectedGenre,
});


const mapDispatchToProps = dispatch => ({
});

export default connect(mapStateToProps, mapDispatchToProps)(Films);
