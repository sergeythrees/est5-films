# EST5 TV films

## Установка

1. Установить Node.js
2. Установка зависимостей `npm i`
3. Запуск режима разработки `npm run start`

## Запуск Docker контейнера

1. docker build -t ps2019tv/est5-films .
2. docker run --rm -it -p 3000:3000 ps2019tv/est5-films

## Сборка проекта

1. Настроить конфигурацию API в `src/config.json`
2. `npm run build`