# 1. Первичное архитектурное решение

Date: 2019-02-10

## Status

Принято

## Context

Мы должны обсудить направление разработки и архитектуру будущего программного решения

## Decision

Составлено планирование проекта: https://docs.google.com/document/d/1DomWbx7K7EQ0OuZBN6rKbv1NlmYp3UQZNoqM9U76USE/edit

## Consequences

Планирование проекта повлияло на предложение для клиента