# 4. Решение о применении Bitbucket Pipiline

Date: 2019-04-01

## Status

Принято

## Context

Для удобства сборки и доставки приложения можно использовать Bitbucket Pipiline

## Decision

Настройка Bitbucket Pipiline

## Consequences

Простой деплой приложения по нажатию кнопки или автоматически